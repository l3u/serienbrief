# SPDX-FileCopyrightText: 2008-2022 Tobias Leupold <tl at stonemx dot de>
#
# SPDX-License-Identifier: BSD-3-Clause

include settings.mk

DIRS = po doc bin

all:
	@echo -e "\nMaking $(PGOAL) ...\n"
	@for n in . $(DIRS); do \
		[ "$$n" = "." ] || $(MAKE) -C $$n || exit $$? ;\
	done
	@echo -e "\nGood -- run make install now.\n"

install: $(ALL)
	@echo -e "\nInstalling $(PGOAL) ...\n"
	@for n in $(DIRS); do \
		(cd $$n; $(MAKE) install) \
	done
	@echo -e "\nFinished installing $(PGOAL)!\n"

clean:
	@echo -e "\nCleaning $(PGOAL) ...\n"
	@for n in $(DIRS); do \
		(cd $$n; $(MAKE) clean) \
	done
	@echo -e "\nFinished cleaning $(PGOAL)!\n"
