# serienbrief

## Create form letters with LaTeX

serienbrief is a Perl script that makes it easy to create form letters using LaTeX. It uses a simple text-only data file with one data set per line. The first line defines the column names, the columns are separated by tabs.

## Homepage

The project's official homepage with further information is <https://nasauber.de/opensource/serienbrief/>.

## Documentation

The documentation is found in `doc/serienbrief.1` and will be accessible via `man serienbrief` when the installation has been done. The `doc` directory also contains an example LaTeX document with an example database.

## Installation

First check if your distribution already has a serienbrief package. At the moment of this writing, Gentoo Linux has one. If you can't install serienbrief via your distribution's package manager, you have to do it manually.

To install serienbrief, simply run `make` (this can be done as a normal user) and `make install` as root afterwards.

This will install the following files:

    /usr/share/locale/de/LC_MESSAGES/serienbrief.mo
    /usr/share/man/man1/serienbrief.1
    /usr/bin/serienbrief

The following Perl modules are used:

    Getopt::Long
    Term::ANSIColor
    Locale::Messages
    POSIX

Be sure to have them installed on your system (and of course Perl itself)!
