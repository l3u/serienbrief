.. SPDX-FileCopyrightText: 2005-2022 Tobias Leupold <tl at stonemx dot de>

   SPDX-License-Identifier: CC-BY-SA-4.0

   The format of this file is inspired by keepachangelog.com, but uses ReStructuredText instead of
   MarkDown. Keep the line length at no more than 100 characters (with the obvious exception of the
   header template below, which needs to be indented by three spaces)

   Here's the header template to be pasted at the top after a new release:

   ====================================================================================================
   [unreleased]
   ====================================================================================================

   Added
   =====

   * for new features.

   Changed
   =======

   * for changes in existing functionality.

   Deprecated
   ==========

   * for soon-to-be removed features.

   Removed
   =======

   * for now removed features.

   Fixed
   =====

   * for any bug fixes.

   Security
   ========

   * in case of vulnerabilities.

====================================================================================================
[unreleased]
====================================================================================================

Added
=====

 * for new features.

Changed
=======

 * Rewrote the old ChangeLog entries and switched to a keepachangelog.com syle ChangeLog format.

 * Switched to SPDX license headers.

 * Relicensed Serienbrief to "GPL 3 or later".

Deprecated
==========

 * for soon-to-be removed features.

Removed
=======

 * for now removed features.

Fixed
=====

 * for any bug fixes.

Security
========

 * in case of vulnerabilities.

====================================================================================================
Serienbrief 0.2.5 released (2014-08-15)
====================================================================================================

 * Reworked the sources, reworked the coding style.

 * Sanitized the l10n.

 * Added the possibility to define custom output file names via the newly introduced "--outpattern"
   option.

 * Skip empty lines in the database file (thanks to Thorsten Grothe for the bug report!).

 * Fixed problems with Windows style line ends in database files on Windows (also thanks to Thorsten
   Grothe for the bug report!).

====================================================================================================
Serienbrief 0.2.4 released (2012-12-29)
====================================================================================================

 * Fixed problems with complex documents that have been probably caused by colliding aux files. Each
   form letter is now processed in an individual temp file.

 * Added the --repeat command line option so that LaTeX can be called multiple times, e.g. to get
   cross-references right.

 * Translated the manpage to a rst document that can be re-translated to a manpage again.

 * Updated the documentation and German translation.

====================================================================================================
Serienbrief 0.2.3 released (2012-05-22)
====================================================================================================

 * Create temporary files in the current directory (not in /tmp) so that external references (like
   images, etc.) will also be found by the processed source files.

 * Updated the documentation.

====================================================================================================
Serienbrief 0.2.2 released (2010-06-01)
====================================================================================================

 * Stopped removing comments, as this breaks some LaTeX documents. The fix has been around for quite
   a while now ...

 * Removed the --tmp-prefix option in favor of simply using files in /tmp, so all tmp files can be
   safely removed.

 * Better output names for the form letters: e.g. name-001.pdf

 * Updated the documentation and German translation.

 * ChangeLog: Fixed old entries.

====================================================================================================
Serienbrief 0.2.1 released (2008-09-23)
====================================================================================================

 * Added the --pretend parameter (thanks to Oleg Fiksel :-)

 * Cleaned up the whole code.

 * Translated the variables to English words.

 * Added a makefile driven installation which creates the l10n on the target system.

====================================================================================================
Serienbrief 0.2 released (2006-07-21)
====================================================================================================

 * Due to a lot of downloads of this script, I decided to translate the the sources to English and
   create a German localization for serienbrief.

 * Pressing CTRL+C when the form letters are created is now handled.

 * Added a manpage.

====================================================================================================
Serienbrief 0.1.1 released (2006-07-19)
====================================================================================================

 * Made it possible to use all kinds of delimiters.

 * Several small bugfixes.

====================================================================================================
Serienbrief 0.1 released (2005-08-11)
====================================================================================================

 * Initial release
