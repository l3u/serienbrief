# SPDX-FileCopyrightText: 2008-2022 Tobias Leupold <tl at stonemx dot de>
#
# SPDX-License-Identifier: BSD-3-Clause

PREFIX = /usr
DATADIR = $(PREFIX)/share
BINDIR = $(PREFIX)/bin
MANDIR = $(DATADIR)/man
LOCALEDIR = $(DATADIR)/locale
PGOAL = serienbrief
